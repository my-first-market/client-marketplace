export const SIGNUP_USER = 'SIGNUP_USER'
export const SIGNIN_USER = 'SIGNIN_USER'
export const UPDATE_USER = 'UPDATE_USER'
export const SIGNOUT_USER = 'SIGNOUT_USER'

export const ADD_TO_CART = 'ADD_TO_CART'
export const CLEAR_CART = 'CLEAR_CART'
export const UPDATE_CART = 'UPDATE_CART'

export const CHANGE_THEME = 'CHANGE_THEME'

