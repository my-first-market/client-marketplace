import React, { useState, useCallback } from "react";
import Wrapper from "./components/Wrapper";
import body from "./assets/body.png";
import sous_one from "./assets/sous-1.png";
import sous_two from "./assets/sous-2.png";
import cucumber from "./assets/cucumber.png";
import tomato from "./assets/tomatos.png";
import kukuruza from "./assets/kukuruza.png";
import becon from "./assets/becon.png";
import chicken from "./assets/chicken.png";
import mashroooms from "./assets/mashrooms.png";
import cheeze from "./assets/cheeze.png";
import { createStyles, makeStyles, Theme, Button, Container } from "@material-ui/core";
import { xor } from "lodash";

const pics = [
  {
    id: 0,
    name: "Толстое тесто",
    src: body
  },
  {
    id: 1,
    name: "Чесночный соус",
    src: sous_one
  },
  {
    id: 2,
    name: "Томатный соус",
    src: sous_two
  },
  {
    id: 3,
    name: "Огурец",
    src: cucumber
  },
  {
    id: 4,
    name: "Помидор",
    src: tomato
  },
  {
    id: 5,
    name: "Кукуруза",
    src: kukuruza
  },
  {
    id: 6,
    name: "Бекон",
    src: becon
  },
  {
    id: 7,
    name: "Курица",
    src: chicken
  },
  {
    id: 8,
    name: "Шампиньоны",
    src: mashroooms
  },
  {
    id: 9,
    name: "Сыр",
    src: cheeze
  }
];

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: "500px",
      width: "60%",
      position: "relative"
    },
    img: {
      position: "absolute",
      top: 0,
      left: 0,
      width: "400px",
      height: "250px"
    },
    wrapperBtns: {
      display: "flex",
      flexDirection: "column"
    },
    btn: {
      margin: "5px"
    }
  })
);

const ConstructorPizza = () => {
  const [selected, setSelected] = useState([0]);
  const onSelect = useCallback(
    number => {
      setSelected(xor(selected, [number]));
    },
    [selected]
  );
  console.log(selected);
  const classes = useStyles();
  return (
    <Container style={{paddingTop: 50}}>
    <Wrapper>
      <div className={classes.root}>
        <img src={pics[0].src} className={classes.img} />
        {pics.map((pic, index) => {
          return selected.includes(index) ? (
            <img
              src={pic.src}
              alt={pic.name}
              className={classes.img}
              style={{ zIndex: index + 1 }}
            />
          ) : null;
        })}
      </div>
      <div style={{ width: "40%" }}>
        <div>
          {pics.map((pic, index) => (
            <Button
              className={classes.btn}
              color="primary"
              variant={selected.includes(index) ? "contained" : "outlined"}
              onClick={() => onSelect(index)}
            >
              {pic.name}
            </Button>
          ))}
        </div>
      <Button>Добавить в корзину</Button>
      </div>
    </Wrapper>
    </Container>
  );
};

export default ConstructorPizza;
