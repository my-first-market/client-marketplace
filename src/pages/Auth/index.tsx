import React from 'react';
import { Grid } from '@material-ui/core';
import FullWidthTabs from 'components/AuthForm/components/Tabs';

const Auth = () => {
  return (
    <Grid container alignItems="center" justify='center'>
      <FullWidthTabs />
    </Grid>
  );
}

export default Auth
