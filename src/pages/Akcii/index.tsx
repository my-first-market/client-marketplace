import React from 'react'
import { Container, makeStyles, Theme, createStyles, Typography } from '@material-ui/core'
import RoomIcon from '@material-ui/icons/Room';
import PhoneIcon from '@material-ui/icons/Phone';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import EmailIcon from '@material-ui/icons/Email';
import { YMaps, Map, Placemark } from 'react-yandex-maps';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrap: {
      display: 'flex',
      justifyContent: 'space-between',
      marginTop: 30,
    },
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    icon: {
      color: '#ffa500',
      width: 50,
      height: 50,
    },
    li: {
      fontSize: 20,
    }
  })
);

const Akc = () => {
  const classes = useStyles()
  return (
    <Container style={{paddingTop: 30}}>
      <Typography variant='h3'>Акции</Typography>
      <ul>
      <li className={classes.li}>
        Выпечка после 20:00 со скидкой 30%
      </li>
      <li className={classes.li}>
         При покупке двух пирогов с повидлом, третий в подарок
      </li>
      </ul>
    </Container>
  )
}

export default Akc