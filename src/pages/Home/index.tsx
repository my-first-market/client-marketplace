import React, { useCallback, useState } from "react";
import {
  makeStyles,
  createStyles,
  Theme,
  Container,
  Typography
} from "@material-ui/core";
import img from "./assets/5.jpg";
import { Animated } from "react-animated-css";
import ShopImages from "./components/ShopImages";
import Particles from "react-particles-js";
import AnimateBubbles from "components/AnimateBubbles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      position: "relative"
    },
    root: {
      width: "100%",
      backgroundImage: `url(${img})`,
      height: 'calc(100vh - 64px)',
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top center",
      paddingTop: 300
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular
    },
    h2: {
      color: "#fff",
      marginTop: 90,
      fontWeight: 700
    },
    h3: {
      color: "#fff",
      marginTop: 30,
      fontWeight: 600
    },
    btn: {
      width: 200,
      textAlign: "center",
      marginTop: 50
    },
    wrap: {
      display: "flex",
      justifyContent: "center",
      marginTop: 20
    },
    line: {
      width: 400,
      height: 7,
      backgroundColor: "#ffa500"
    },
    icon: {
      color: "#fff",
      marginTop: 30
    }
  })
);

const Home: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.root}>
        <Container style={{ paddingTop: "30px" }}>
          <Animated
            animationIn="fadeInRight"
            animationOut="fadeOut"
            isVisible={true}
            animationInDuration={4500}
          >
            <Typography variant="h2" align="center" className={classes.h2}>
              Пекарня "Горячий хлеб"
            </Typography>
          </Animated>
          <Animated
            animationIn="fadeInLeft"
            animationOut="fadeOut"
            isVisible={true}
            animationInDuration={4500}
          >
            <Typography variant="h3" align="center" className={classes.h3}>
              Вкусная выпечка по традиционным рецептам
            </Typography>
          </Animated>
          <Animated
            animationIn="bounce"
            animationOut="fadeOut"
            isVisible={true}
            animationInDuration={4500}
          >
            <div className={classes.wrap}>
              <div className={classes.line}></div>
            </div>
          </Animated>
        <AnimateBubbles />
        </Container>
 
      </div>
      <ShopImages />
    </div>
  );
};
export default Home;
