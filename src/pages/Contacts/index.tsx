import React from 'react'
import { Container, makeStyles, Theme, createStyles, Typography } from '@material-ui/core'
import RoomIcon from '@material-ui/icons/Room';
import PhoneIcon from '@material-ui/icons/Phone';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import EmailIcon from '@material-ui/icons/Email';
import { YMaps, Map, Placemark } from 'react-yandex-maps';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrap: {
      display: 'flex',
      justifyContent: 'space-between',
      marginTop: 30,
    },
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    icon: {
      color: '#ffa500',
      width: 50,
      height: 50,
    }
  })
);

const Contacts = () => {
  const classes = useStyles()
  return (
    <Container style={{paddingTop: 30}}>
      <Typography variant='h3'>Наши контакты</Typography>
      <div className={classes.wrap}>
        <div className={classes.item}>
          <RoomIcon className={classes.icon} />
          <Typography>
          Россия, Алтайский край,<br />
          Мамонтово (Мамонтовский район)
          </Typography>
        </div>
        <div className={classes.item}>
          <PhoneIcon className={classes.icon}  />
          <Typography>
          +7 (999) 999 99 99
          </Typography>
        </div>
        <div className={classes.item}>
          <QueryBuilderIcon className={classes.icon}  />
          <Typography>
          C 10-00 до 21-00
          </Typography>
        </div>
        <div className={classes.item}>
          <EmailIcon className={classes.icon}  />
          <Typography>
          test@mail.ru
          </Typography>
        </div>
      </div>
      <YMaps>
    <div style={{width: '100%'}}>
      <Map style={{width: '100%', height: 500, marginTop: 50}} defaultState={{ center: [52.7139, 81.6194], zoom: 17 }}>
      <Placemark geometry={[52.7139, 81.6194]} options={{
            iconColor: '#ffa500'
      }} />
      </Map>
    </div>
  </YMaps>
    </Container>
  )
}

export default Contacts