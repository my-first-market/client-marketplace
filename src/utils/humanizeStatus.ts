const humanizeStatus = (status: string) => {
    switch(status) {
        case 'pending':
            return 'Проверка оплаты'
        case 'disabled':
            return 'Отменен'
        case 'paid':
            return 'В доставке'
        case 'deleted':
            return 'Удален'
        case 'closed':
            return 'Завершен'
        case 'active':
            return 'Принят'
        case 'declined':
            return 'Отказ'
        case 'sent':
            return 'Отправлен'
        case 'delivered':
          return 'В доставке'
        case 'error':
            return 'Ошибка оплаты'
        default:
            return ''
    }
  }
  export default humanizeStatus
  