export const validate = (values: any) => {
  const errors: any = {};
  if (!values.address) {
    errors.address = 'Обязательное поле';
  }
  if (!values.phone_mobile) {
    errors.phone_mobile = 'Обязательное поле';
  }
  return errors;
};