import React from 'react'
import Particles from 'react-particles-js'

interface IProps {
  children?: React.ReactNode,
  style?: any,
}
const AnimateBubbles: React.FC<IProps> = ({children, style}) => (
  <Particles
  style={style}
  params={{
    particles: {
      number: {
        value: 300,
        density: {
          enable: false
        }
      },
      size: {
        value: 3,
        random: true,
        anim: {
          speed: 4,
          size_min: 1
        }
      },
      line_linked: {
        enable: false
      },
      move: {
        random: true,
        speed: 1,
        direction: "top",
        out_mode: "out"
      }
    },
    interactivity: {
      events: {
        onhover: {
          enable: true,
          mode: "bubble"
        },
        onclick: {
          enable: true,
          mode: "repulse"
        }
      },
      modes: {
        bubble: {
          distance: 150,
          duration: 2,
          size: 5,
          opacity: 0
        },
        repulse: {
          distance: 400,
          duration: 4
        }
      }
    }
  }}
>
  {children}
</Particles>
)

export default AnimateBubbles