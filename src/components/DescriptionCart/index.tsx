import React, { useCallback, useEffect, useMemo, useState } from "react";
import {
  Typography,
  Button,
  createStyles,
  makeStyles,
  Theme,
  Grid,
  IconButton,
  ListItem,
  Avatar,
  ListItemAvatar,
  TextField
} from "@material-ui/core";
import { IProduct, ICartItem, IUser } from "react-app-env";
import { useSelector } from "react-redux";
import { get, omit } from "lodash";
import useCart from "hooks/useCart";
import Rating from "@material-ui/lab/Rating";
import Availability from "./components/Availability";
import { useHistory, Redirect, Link } from "react-router-dom";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    margin: {
      margin: theme.spacing(2)
    },
    extendedIcon: {
      marginRight: theme.spacing(1)
    },
    wrap: {
      marginTop: 20,
      marginLeft: 50
    },
    avatar: {
      display: "flex",
      alignItems: 'center',
      marginTop: 5,
      marginBottom: 5,
    },
    textWrap: {
      marginLeft: 10,
      color: '#000000',
      textDecoration: 'none',
    },
    text: {
      color: '#000000',
      textDecoration: 'none',
    },
    btns: {
      // borderRadius: '50%',
      // width: 10,
      // height: 40
    },
    field: {
      width: 50,
      textAlign: 'center',
      border: 'none',
    },
    title: {
      marginBottom: 10,
    }
  })
);

interface IProps {
  product: IProduct;
}
const DescriptionCart = ({ product }: IProps) => {
  const [count, setCount] = useState(1)
  let history = useHistory()
  const isAuth: IUser = useSelector((store: any) => get(store, 'auth.isAuth', []))

  const classes = useStyles();
  const { addItem, items } = useCart();
  const Ids = items.map(item => item.product.id);
  const isInclude = Ids.includes(product.id);

  const item = { product, count: count, id: product.id };
  const onAddCart = useCallback(() => {
    isAuth ?
    addItem(item) 
    : history.push('/auth');
  }, [item, items]);
  
  const buyNow = useCallback(async () => {
    if(isAuth) {
      addItem(item)
      history.push('/cart')
    } else {
      history.push('/auth')
    }
  }, [item, items]);

  const onChange = useCallback((event: any) => {
    setCount(Number(event.target.value))
  }, [count])

  console.log(count)
  const plus = useCallback(() => {
    setCount(count + 1)
  }, [count])

  const minus = useCallback(() => {
    if( count <= 0) return
    setCount(count - 1)
  }, [count])
  return (
    <>
      <Grid container className={classes.wrap}>
        <Grid item xs={12}>
          <Typography variant="h4" className={classes.title}>{product.name}</Typography>
          <Link style={{textDecoration: 'none'}} to={`/shop/${product.shop.id}`} className={classes.avatar}>
            <Avatar src={product.shop && product.shop.logo && product.shop.logo.src} />
            <div className={classes.textWrap}>
              <Typography className={classes.text}>{product.shop.name}</Typography>
            </div>
          </Link>
          <Rating name="read-only" value={product.rating} readOnly />
          <div style={{ fontSize: 16 }}>Оценка товара: {product.rating} из 5</div>
          {/* <Availability amount={product.amount} /> */}
          <Typography gutterBottom variant="h6">
            {product.price}
            {product.currency.name}
          </Typography>
        </Grid>
        <Grid>
          <Button className={classes.btns} onClick={minus}>-</Button>
          <TextField className={classes.field} type='number' value={count} defaultValue={1} onChange={onChange} />
          <Button className={classes.btns} onClick={plus}>+</Button>
        </Grid>
        <Grid item xs={12}>
          <Button onClick={buyNow} disabled={!count} variant="contained" size="large" color="primary">
            Купить сейчас
          </Button>
          <Button
            onClick={onAddCart}
            variant="outlined"
            size="large"
            color="primary"
            disabled={!count} 
            className={classes.margin}
          >
            {isInclude ? `Добавить еще раз` : `Добавить в корзину`}
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default DescriptionCart;
